/* javascrpit operators */  // variable + operators = Expression(Algorithm)

// Assignment Operators // = - Assignment

 
let x = 5;
 
x++
x = x+2;

x *= 5

console.log(x)

// Arithmetic Operators 

let number5 = 17;
let number6= 03;

// let number7 = (number5 + number6);
console.log(number5  + number6)
console.log(number5  - number6)
console.log(number5  * number6)
console.log(number5  / number6)
console.log(number5  % number6)
console.log(number5  ** number6)

// Increment  ()

// console.log(++number5)
// console.log(number5++)
// console.log(number5)


//Decrement()
console.log( 'vvv',number5--)
console.log('bbb',number5) 

/* Comparison operators) */

let M = 5


//reational operators


// console.log(M > 5) // Greater than
// console.log(M >= 5) // Greater than or equal to

// console.log(M < 5) // less than
// console.log(M <= 5) // less than or equal to

// Equality opertors

console.log(M === 5)
console.log(M !== 5)


// string comparison

console.log('JAGAB' > 'SURYC'); // Dictionary mode cal


// Comparsion of different type

console.log('1' < 5)
 console.log(true == 1)   //  1-true ,  0- false
 console.log( '1' === 1)


 // Strict Equality Opertor ( Datatpe + Value Checking)

 console.log( 1 === 1)
 console.log( '1' === 1)

 //lose Equality Operator

 console.log( 1 == 1)
 console.log( '1' == 1)
 console.log(true ==1)


 /* Ternary Operator */

 let age = 24 ;
 if (age>18){
 let type ='jagan'
 }else{
   let type = 'surya'
 }

 // condition ? 'value' : 'value2'
 
let ages = 171;
 let type = ages > 18 ? "jagan" : 'Surya';
 console.log(type)


 /* Logical Operators */

 // Logical AND (&&)
 // returns TRUE  if both operands are True

 console.log(true && true)
 console.log(true && false)

  // Logical OR (||)
 // returns TRUE  if Anyone  operands are True

 console.log(true || true)
 console.log(true || false)

  // Logical NOT (!)
 // returns TRUE  if Anyone  operands are True

 console.log(!true )
 console.log(!!true)

 let highIncome = true;
 let CIBILScore = true;

 let customer = highIncome && CIBILScore;
 let customer1 = highIncome || CIBILScore;
 
 let applicationstatus = !customer
 
 console.log( "status : " + customer );
 console.log( "status : " + customer1 );
 console.log( "status : " +   applicationstatus);


 // Falsy (false)
 //undefined,
 //null,
 //0,
 //false
 //'' ->"",
 //NaN

 // Truty -> Anything that is not falsy is ->Truthy

 console.log(false || "jagan")
 console.log(false || 1)
 console.log(false || true)
 console.log(false || 0)
 

 let userColor = 'red' ;
 let defaultColor = 'blue';
  let color = userColor || defaultColor;
  
  console.log(color)


  /* BtWise Operators */

  // Humancode -> Machine code (0,1)

  // 1 -> 00000001 -> 1
  // 2 -> 00000010 -> 2

  console.log(1|2);
  console.log(1&2);

  const readPrmission = 4;
  const writePermission = 2;
  const executePerission = 1;

let myPermission = 0

myPermission = myPermission | writePermission |readPrmission ;

let message  = (myPermission & readPrmission) ? 'yes' : 'no';
console.log(message )

