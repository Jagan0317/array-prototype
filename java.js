
/* Concat */
const array1 = [1,2,3,4];
const array2 = [5,6,7,8];
const array3 = [9,10,11,12];

const array = array1.concat(array2.concat(array3));
console.log('concat',array)



/* Find */
const find1  = [5, 12, 8, 130, 44];
const find = find1.find(element =>element >44)
console.log( "find",find)


/* Filder */
const filder1  = [5, 12, 8, 130, 44];
const filder = filder1.filter(element =>element >5)
console.log( "filder",filder)


let filder2  = ['jagan', 'jagadesh','surya','abilash','selvavijay','vinothkumar']
let filder3 = filder2.filter(word =>word.length>5)
console.log('filder',filder3)


/* push - pop*/
const push = [1,2,3,4,5,6,7,8]
const push1 = push.push(9,10);
console.log('push: Add last' ,push)

let pop  = ['jagan', 'jagadesh','surya','abilash','selvavijay','vinothkumar']
let pop1 = pop.pop();
console.log('pop',pop1)
console.log('pop:Remove last',pop)

/* shift - unshift */

const shift = [1,2,3,4,5,6,7,8]
const shift1 = shift.shift()
console.log('shift: REmove first',shift)

const unshift = [1,2,3,4,5,6,7,8]
const unshift1 = unshift.unshift(0,9)
console.log('unshift: Add first ',unshift)

/* slice - splice */ 

const splice = ['Jan', 'March', 'April', 'july'];
const splice1 = splice.splice(3,0,'june')
const splice3 = splice.splice(1,1)
console.log('SPLICE, ADD REMOVE REPLACE ',splice)

let slice1  = ['jagan', 'jagadesh','surya','abilash','selvavijay','vinothkumar']
let slice2 = slice1.slice(2,-1)
console.log('slice: Start End',slice2)

/* Some - Every */ 

const fruits = ['apple', 'banana', 'mango', 'guava'];

function checkAvailability(array,value){
    return array.some((arrayvalue)=> value  === arrayvalue);
}
console.log(checkAvailability(fruits,'Orange'))
console.log(checkAvailability(fruits,'mango'))

const fruits1 = ['apple', 'banana', 'mango', 'guava'];

function checkAvailability(array,value){
    return array.every((arrayvalue)=> value  === arrayvalue);
}
// console.log(checkAvailability(fruits1,'Orange'))
console.log(checkAvailability(fruits1,'apple'))


/* map - forEach */ 

const map1 = [1,3,6,8,4];
const map2 = map1.map(x=>x*2)
console.log(map2)


const map3 = ['jsgsn', 'surya','abilash']



var people = [
    { id: 1, firstname: "jagan", lastname: "raj" },
    { id: 2, firstname: "suriya", lastname: "ks" },
    { id: 3, firstname: "sara", lastname: 'sri'}
];

// let final = people.map(element=>element.name)

// let final = people.map(function(value){
//     return value.firstname + value.last
// })

let final = people.map(function(value){
    // let fullname = value.firstname + value.lastname;
    let fullname = [value.firstname, value.lastname].join(' ');
    let obj ={id:value.id, fullname:fullname}
    return obj;
})

console.log(final)



const foreach1 = ['a', 'b', 'c'];
const foreach2 = foreach1.forEach(element=>element.foreach1)
foreach1.forEach(element => console.log(element))
// console.log(foreach1)


const jagan = [1,4,7,];
let number  = 5;

jagan.forEach((element)=>{
    console.log({element});
    number++;
})

console.log({number})



/*arry of object filder */
var office = [
    { id: 1, disignation: "trainee", rating: 5 },
    { id: 2, designation: "developer", rating: 3 },
    { id: 3, designation: "senior developer", rating: 1 }
];

let OurOffice =  office.filter((office)=>{
    return office.rating>1
});
console.log(OurOffice)



/*array of object find */
var ObjFind = [
    { id: 1, name: "jagan", gender: "male" },
    { id: 2, name: "suriya", gender: "male" },
    { id: 3, name: "ansi", gender: "female" }
];


const ObjFind1 = ObjFind.find((ObjFind) => {
    return ObjFind.id = 2;
});
console.log(ObjFind1);


/*lenght*/

let name  = ['jagan', 'jagadesh','surya','abilash','selvavijay','vinothkumar']

console.log(name.length)

/*includes*/

const sreach =['cat', 'dog', 'bat'];
let search = sreach.includes('tiger');
// let search = sreach.includes('dog');
console.log(search)


/*indexof*/

let names  = ['jagan', 'jagadesh','surya','abilash','selvavijay','vinothkumar']
const names1 = names.indexOf('selvavijay')
console.log('indexof',names1)


let index =[1,2,4,5,6,7,8,3]
let index1 = index.indexOf(3)
console.log('index',index1)


/*lastindexof*/
const animals = ['Dodo', 'Tiger', 'Penguin', 'Dodo'];
let animals1 = animals.lastIndexOf('Dogo')
console.log(animals1)




const numbers =[1,2,5,8,-3,9 ,4]


let jagan1 = numbers
.filter(n=>n >=0).map(n=>({val:n}))
.filter(obj =>obj.val>1)
.map(obj => obj.val)
console.log('yyyy',jagan1)


let fildered = numbers.filter(n=>n>=0)

// let item = fildered.map(n=> '<li>' + n +'</li>')
// let html = item.join()
// console.log(html)



let items = fildered.map(n=>{
    return {value:n}
});
console.log('xxx',items)


let items1 = fildered.map(n=> ({value:n }));
console.log('xxx',items1)



let Items = fildered.map(n=>{
    const obj = {Val:n};
    return obj;
})
console.log('jagan', Items)
console.log('jg', fildered)



/* spread Operator*/


const fruit = ['applae', 'mango','banana']
const fruty = ['orange', 'jackfrut']
const shoppingCard = ['maagi', 'beans','masala']

let spread1 = [...fruit,...shoppingCard,...fruty];
console.log('compaine', spread1)


function sum(x, y, z) {
    return x * y * z;
  }
  
  const numbers1 = [1, 2, 3];
  
  console.log(sum(...numbers1));




  /* rest Operator  method is  used to function */ /*Rest Operator - remaing items */ 

  function sumExpense(loan,...expenses){

    // return expenses.reduce((a,b)=>{
    //     return a + b ;
    // });

    let total = expenses.reduce((a,b)=> a + b );
    return total - loan ;

    // console.log(expenses);
  }
  let result = sumExpense(100,45,85,987,74,57,75);

  console.log(result);



  /* Call Apply Bind method */

//   let method  = {
//     first_name:"jagan",
//     last_name:'raj',
//     printfullName:function(){
//     console.log(this.first_name + " " + this.last_name)
//     }
//   }
// //   method.printfullName();
// console.log(method.printfullName())

// let method2 = {
//     first_name:'jaga',
//     last_name:'desh',
// }

// // function borrwing //
// method.printfullName.call(method2);


let method  = {
    first_name:"jagan",
    last_name:'raj',
  }
 let printfullName=function( hometown, state){
    console.log(this.first_name + " " + this.last_name + " from " +hometown + " ," + state)
    }

//   method.printfullName();
// console.log(method.printfullName())
printfullName.call(method, 'chennai', 'Tamilnadu')

let method2 = {
    first_name:'jaga',
    last_name:'desh',
}

// function borrwing //
printfullName.call(method2 ,'Pondicherry', 'Pondicherry');

printfullName.apply(method2 ,['Pondicherry', 'Pondicherry']);

//BBind method,
 let printMyName = printfullName.bind(method2 ,'Pondicherry', 'Pondicherry');
console.log(printMyName)
printMyName()









